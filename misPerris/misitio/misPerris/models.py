from django.db import models
from django.utils import timezone

# Create your models here.
#CLASES

class post(models.Model):
    autor = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    titulo = models.CharField("Titulo", max_length=200)
    articulo = models.TextField("Articulo")
    fecha_creacion = models.DateTimeField("Fecha Creacion", default=timezone.now)
    fecha_publicacion = models.DateTimeField("Fecha Publicacion", blank=True, null=True) #blanck permite atributos en blanco y null acepta datos nulos

    def publish(self):
        self.fecha_publicacion = timezone.now()
        self.save()
    
    def __str__(self):
        return self.titulo #lo que va mostrar en la grilla
    

    